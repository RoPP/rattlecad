# Contributing guide

This repository contains an entire Eclipse-Workspace for the rattleCAD-Project.

For the initial setup you can use and extract the `.metadata.zip` which
contains my Eclipse-Settings for the project.

In your case it may be necessary to update these settings.

Cheers Manfred
[fredstmk@users.sourceforge.net](mailto:fredstmk@users.sourceforge.net)
