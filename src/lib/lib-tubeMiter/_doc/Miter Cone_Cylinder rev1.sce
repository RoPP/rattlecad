//Schnittkurvenberechnung Kreiszylinder-Kegel
//Eingabedaten
R1=19 //Radius Zylinder in mm
alpha=120*%pi/180 // Schnittwinkel in rad
hks=45 //Kegelhöhe von Kegelfuß bis Achsenschnittpunkt
// Geometrie Columbus Tapered Head Tube
// Das HT hat unten D=56mm und oben 46 mm bei einer Kegelstumpfhöhe von 70mm
R2col_u=28
R2col_o=23
h2col=70
// Dies wird umgerechnet auf einen Kegelfuß im Achsenschnittpunkt und die Kegelhöhe bis zur virtuellen Kegelspitze
h2=h2col/(1-R2col_o/R2col_u)
R20=(1-hks/h2)*R2col_u
//Wertebereich für Z2 ermitteln
//Oberer Sattelpunkt
Z2_top=(R1+R20*cos(alpha))/(R20/h2*cos(alpha)+sin(alpha))
Z2_bot=(-R1+R20*cos(alpha))/(R20/h2*cos(alpha)+sin(alpha))
//Durchführung der Berechnungen mit nz2 Hilfsebenen
//PHI1 läuft zwischen 0 und pi
nz2max=400
for nz2=1:nz2max+1
    Z2(nz2)=Z2_top-(nz2-1)/nz2max*(Z2_top-Z2_bot);
    Z1(nz2)=-sqrt((R20*(1-Z2(nz2)/h2))^2+Z2(nz2)^2-R1^2);
    PHI2(nz2)=acos((-Z2(nz2)*cos(alpha)-Z1(nz2))/(R20*(1-Z2(nz2)/h2)*sin(alpha)));
    PHI1s(nz2)=asin((R20*(1-Z2(nz2)/h2)*sin(PHI2(nz2)))/R1);
    if nz2 > 2 then
        if real(2*PHI1(nz2-1)-PHI1(nz2-2)) < real(%pi/2) then 
            PHI1(nz2)=PHI1s(nz2);
        else
            PHI1(nz2)=%pi-PHI1s(nz2);
        end
    else PHI1(nz2)=PHI1s(nz2)
    end 
end
// Jetzt noch das Ganze spiegeln für den Bereich PHI1 von pi bis 2pi
for nz2=nz2max+2:2*nz2max+1
    Z1(nz2)=Z1(2*nz2max-nz2+2);
    PHI1(nz2)=2*%pi-PHI1(2*nz2max-nz2+2);
end
//Schnittkurve plotten
//Im Diagramm entsprechen der linke und rechte Rand dem oberen Sattel. Die Diagrammmitte entspricht dem unteren Sattel
clf()
//plot(PHI1*R1,Z1)
//Falls der obere Sattel in die Mitte gerückt werden soll, dann folgende Darstelung verwenden:
Z1sort(1:nz2max+1)=Z1(nz2max+1:2*nz2max+1);
Z1sort(nz2max+2:2*nz2max+1)=Z1(1:nz2max);
plot(PHI1*R1,[Z1sort Z1]);
