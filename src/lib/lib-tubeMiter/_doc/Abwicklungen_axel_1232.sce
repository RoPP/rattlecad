//Durchmesser
SaRD=28.6;                  //Sattelrohr/Unterrohr
SaRD2=29.8;                 //Sattelrohr/Oberrohr
TLD=40;
URD=31.7;
ORD=28.6;
StRD=31.7;
//Längen
URL=623.4;
ORL=540.9;
SaRL=545.3;
//Winkel
URStRW=59.7;
SaRURW=59.4;
ORStRW=81.6;
ORSaRW=98.05;

TLD12=TLD/2;
SaRD12=SaRD/2;
URD12=URD/2;
ORD12=ORD/2;
StRD12=StRD/2;
SaRD212=SaRD2/2;

URStRW2rad=(180-URStRW)*%pi/180;
SaRURWrad=SaRURW*%pi/180;
ORSaRW2rad=(180-ORSaRW)*%pi/180;

USaR=%pi*SaRD;
UUR=%pi*URD;
UOR=%pi*ORD;
UStR=%pi*StRD;

n=360;
fd=mopen("d:\abwicklungrohre.scr","w");
//Unterrohr
XAbwUR=0;
YAbwUR=0;
mfprintf(fd,'Linie %3.2f,%3.2f @ %3.2f,%3.2f @ %3.2f,%3.2f @ %3.2f,%3.2f @ %3.2f,%3.2f S\n',XAbwUR,YAbwUR,XAbwUR,URL,UUR,URL,UUR,YAbwUR,XAbwUR,YAbwUR);
mfprintf(fd,'Linie %3.2f,%3.2f',XAbwUR,YAbwUR);
for k=0:10:n,x=XAbwUR+UUR/n*k; y=URL-sqrt(StRD12^2-(URD12*sin(k*%pi/180))^2)/sin(URStRW2rad)-(URD12*cos(k*%pi/180)/tan(URStRW2rad));mfprintf(fd,' @ %3.2f,%3.2f',x,y);end
mfprintf(fd,' S\n');
mfprintf(fd,'Linie %3.2f,%3.2f',XAbwUR,YAbwUR);
for k=0:10:n,x=XAbwUR+UUR/n*k; test=URD12*sin(k*%pi/180);
  if test>SaRD12 then p=0; elseif test*-1>SaRD12 then p=0; else p=1; end
  ysar=YAbwUR+p*(sqrt(SaRD12^2-(URD12*sin(k*%pi/180))^2)/sin(SaRURWrad)+URD12*cos(k*%pi/180)/tan(SaRURWrad)); mfprintf(fd,' @ %3.2f,%3.2f',x,ysar);
end
mfprintf(fd,' S\n');
mfprintf(fd,'Linie %3.2f,%3.2f',XAbwUR,YAbwUR);
for k=0:10:n,x=XAbwUR+UUR/n*k; ytl=YAbwUR+sqrt(TLD12^2-(URD12*cos(k*%pi/180))^2);mfprintf(fd,' @ %3.2f,%3.2f',x,ytl);end
mfprintf(fd,' S\n');

//Oberrohr
XAbwOR=UUR+25;
YAbwOR=0;
X2AbwOR=XAbwOR+UOR;
mfprintf(fd,'Linie %3.2f,%3.2f @ %3.2f,%3.2f @ %3.2f,%3.2f @ %3.2f,%3.2f @ %3.2f,%3.2f S\n',XAbwOR,YAbwOR,XAbwOR,ORL,X2AbwOR,ORL,X2AbwOR,YAbwOR,XAbwOR,YAbwOR);

mfprintf(fd,'Linie %3.2f,%3.2f',XAbwOR,YAbwOR);
for k=0:10:n,x=XAbwOR+UOR/n*k; y=YAbwOR+sqrt(SaRD212^2-(ORD12*sin(k*%pi/180))^2)/sin(ORSaRW2rad)+(ORD12*cos(k*%pi/180)/tan(ORSaRW2rad));mfprintf(fd,' @ %3.2f,%3.2f',x,y);end
mfprintf(fd,' S\n');
mfprintf(fd,'Linie %3.2f,%3.2f',XAbwOR,YAbwOR);
for k=0:10:n,x=XAbwOR+UOR/n*k; y=ORL-sqrt(StRD12^2-(ORD12*sin(k*%pi/180))^2)/sin(ORStRW*%pi/180)+ORD12*cos(k*%pi/180)/tan(ORStRW*%pi/180);mfprintf(fd,' @ %3.2f,%3.2f',x,y);end
mfprintf(fd,' S\n');

//Sattelrohr
XAbwSaR=X2AbwOR+25;
YAbwSaR=0;
X2AbwSaR=XAbwSaR+USaR;

  mfprintf(fd,'Linie %3.2f,%3.2f @ %3.2f,%3.2f @ %3.2f,%3.2f @ %3.2f,%3.2f @ %3.2f,%3.2f S\n',XAbwSaR,YAbwSaR,XAbwSaR,SaRL,X2AbwSaR,SaRL,X2AbwSaR,YAbwSaR,XAbwSaR,YAbwSaR);

  mfprintf(fd,'Linie %3.2f,%3.2f',XAbwSaR,YAbwSaR);
    for k=0:10:n,x=XAbwSaR+USaR/n*k; y=sqrt(TLD12^2-(SaRD12*cos(k*%pi/180))^2);mfprintf(fd,' @ %3.2f,%3.2f',x,y);end
  mfprintf(fd,' S\n');
  mclose(fd);

