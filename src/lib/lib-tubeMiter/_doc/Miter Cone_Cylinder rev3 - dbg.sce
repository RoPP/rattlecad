//Schnittkurvenberechnung Kreiszylinder-Kegel

fd=mopen(".\protocol.txt","w");
//

//Eingabedaten
R_Tube=19 //Radius Zylinder in mm

alpha=120*%pi/180 // Schnittwinkel in rad

hks=45 //Kegelhöhe von Kegelfuß bis Achsenschnittpunkt

// Geometrie Columbus Tapered Head Tube

// Das HT hat unten D=56mm und oben 46 mm bei einer Kegelstumpfhöhe von 70mm

R_Tool_base=28

R_Tool_top=23

H_Tool_Frustum=70

// print (%io(2), R_Tool_base, R_Tool_top, H_Tool_Frustum)

// Dies wird umgerechnet in eine Funktion Kegelradius in Abhängigkeit der Koordinate z2. Zunächst Geradengleichung bestimmen

a2=R_Tool_base+(R_Tool_top-R_Tool_base)*hks/H_Tool_Frustum;

b2=(R_Tool_top-R_Tool_base)/H_Tool_Frustum;

// print (%io(2), a2,b2)

//Wertebereich für Z2 ermitteln. 
//Z2 läuft entlang der Kegelachse vom Achsenschnittpunkt aus positiv nach oben
//Z1 läuft entlang der Zylinderachse. Vom Achsenschnittpunkt aus von links nach rechts ist positiv. Daher werden im Ergebnis negative Werte ausgespuckt, denn die Schnittkurve liegt ja links vom Achsenschnittpunkt. 
//Oberer Sattelpunkt

Z2_top=(R_Tube+a2*cos(alpha))/(-b2*cos(alpha)+sin(alpha))
//unterer Sattelpunkt
Z2_bot=(-R_Tube+a2*cos(alpha))/(-b2*cos(alpha)+sin(alpha))

print (%io(2), Z2_top, Z2_bot)

//Durchführung der Berechnungen mit Hilfsebenen, die senkrecht zur Kegelachse verlaufen. Vom oberen Sattel bis zum unteren sind es nz2max+1 Hilfsebenen. Der Laufparameter dafür ist nz2 und zu jedem nz2 wird die entsprechende z2-Koordinate ermittelt, also in welchem Abstand sich die Hilfsebene zum Achsenschnittpunkt befindet. 
//Dabei wandert der Winkel PHI1 für den Zylinder zwischen 0 und pi. Später wird das Ganze gespiegelt, um den Bereich zwischen pi und 2pi abzudecken. PHI1s ist eine Berechnung von PHI1, allerdings fehlt dort die korrekte Berücksichtigung des Quadranten, da die Arkusfunktion dies nicht kann. In einem weiteren Schritt wird dann der Quadrant festgestellt, um auch Werte >pi/2 erhalten zu können.
//PHI2 ist der Umlaufwinkel am Kegel, siehe auch Skizze in der theoretischen Herleitung. 

mfprintf(fd,'R_Tube:         %3.8f\n', R_Tube);
mfprintf(fd,'alpha:          %3.8f\n', alpha);
mfprintf(fd,'hks:            %3.8f\n', hks);
mfprintf(fd,'R_Tool_base:    %3.8f\n', R_Tool_base);
mfprintf(fd,'R_Tool_top:     %3.8f\n', R_Tool_top);
mfprintf(fd,'H_Tool_Frustum: %3.8f\n', H_Tool_Frustum);
mfprintf(fd,'a2:             %3.8f\n', a2);
mfprintf(fd,'b2:             %3.8f\n', b2);
mfprintf(fd,'-----------------------------\n');

nz2max=400

mfprintf(fd,'nz2max:         %3.8f\n', nz2max);
mfprintf(fd,'-----------------------------\n');

for nz2=1:nz2max+1

    mfprintf(fd,'\n---- %d -------------------------------------------\n', nz2);
    
    Z2(nz2)=Z2_top-(nz2-1)/nz2max*(Z2_top-Z2_bot);
    mfprintf(fd,'       Z2(%d):     %3.8f\n', nz2, Z2(nz2));
   
    Z1(nz2)=-sqrt((a2+b2*Z2(nz2))^2+Z2(nz2)^2-R_Tube^2);
    mfprintf(fd,'       Z1(%d):     %3.8f\n', nz2, Z1(nz2));
    
    PHI2(nz2)=acos((-Z2(nz2)*cos(alpha)-Z1(nz2))/((a2+b2*Z2(nz2))*sin(alpha)));
    mfprintf(fd,'       PHI2(%d):   %3.8f\n', nz2, PHI2(nz2));
    
    PHI1s(nz2)=asin((a2+b2*Z2(nz2))*sin(PHI2(nz2))/R_Tube);
    mfprintf(fd,'       PHI1s(%d):  %3.8f\n', nz2, PHI1s(nz2));
    
    // print (%io(2), Z2(nz2), Z1(nz2), PHI2(nz2),  PHI1s(nz2)) 
    
    if nz2 > 2 then

        if real(2*PHI1(nz2-1)-PHI1(nz2-2)) < real(%pi/2) then 
            mfprintf(fd,'       -> if:\n');
            PHI1(nz2)=PHI1s(nz2);
        else
            mfprintf(fd,'       -> else:\n');
            PHI1(nz2)=%pi-PHI1s(nz2);
        end
    mfprintf(fd,'       PHI1(%d):   %3.8f\n', nz2, PHI1(nz2));

    else PHI1(nz2)=PHI1s(nz2)

    end 

end

// Jetzt noch das Ganze spiegeln für den Bereich PHI1 von pi bis 2pi

mfprintf(fd,'\n\n\n');
mfprintf(fd,'---- mirror -------------------------\n');

for nz2=nz2max+2:2*nz2max+1

    Z1(nz2)=Z1(2*nz2max-nz2+2);
    mfprintf(fd,'       Z1(%d):     %3.8f\n', nz2, Z1(nz2));
 
    PHI1(nz2)=2*%pi-PHI1(2*nz2max-nz2+2);
    mfprintf(fd,'       PHI1(%d):   %3.8f\n', nz2, PHI1(nz2));

end
mfprintf(fd,'\n\n\n');

//Schnittkurve plotten

//Im Diagramm entsprechen der linke und rechte Rand dem oberen Sattel. Die Diagrammmitte entspricht dem unteren Sattel

clf()

//plot(PHI1*R_Tube,Z1)

//Falls der obere Sattel in die Mitte gerückt werden soll, dann folgende Darstelung verwenden:

Z1sort(1:nz2max+1)=Z1(nz2max+1:2*nz2max+1);

Z1sort(nz2max+2:2*nz2max+1)=Z1(1:nz2max);

plot(PHI1*R_Tube,[Z1sort Z1]);

mclose(fd);
