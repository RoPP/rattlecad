#!/bin/sh
# the next line restarts using wish \
exec wish "$0" "$@"


 ##+##########################################################################
 #
 # osEnv.tcl
 #
 #   osEnv is software of Manfred ROSENBERGER
 #       based on tclTk and BWidgets and their 
 #       own Licenses.
 # 
 # Copyright (c) Manfred ROSENBERGER, 2016/01/01
 #
 # The author  hereby grant permission to use,  copy, modify, distribute,
 # and  license this  software  and its  documentation  for any  purpose,
 # provided that  existing copyright notices  are retained in  all copies
 # and that  this notice  is included verbatim  in any  distributions. No
 # written agreement, license, or royalty  fee is required for any of the
 # authorized uses.  Modifications to this software may be copyrighted by
 # their authors and need not  follow the licensing terms described here,
 # provided that the new terms are clearly indicated on the first page of
 # each file where they apply.
 #
 # IN NO  EVENT SHALL THE AUTHOR  OR DISTRIBUTORS BE LIABLE  TO ANY PARTY
 # FOR  DIRECT, INDIRECT, SPECIAL,  INCIDENTAL, OR  CONSEQUENTIAL DAMAGES
 # ARISING OUT  OF THE  USE OF THIS  SOFTWARE, ITS DOCUMENTATION,  OR ANY
 # DERIVATIVES  THEREOF, EVEN  IF THE  AUTHOR  HAVE BEEN  ADVISED OF  THE
 # POSSIBILITY OF SUCH DAMAGE.
 #
 # THE  AUTHOR  AND DISTRIBUTORS  SPECIFICALLY  DISCLAIM ANY  WARRANTIES,
 # INCLUDING,   BUT   NOT  LIMITED   TO,   THE   IMPLIED  WARRANTIES   OF
 # MERCHANTABILITY,    FITNESS   FOR    A    PARTICULAR   PURPOSE,    AND
 # NON-INFRINGEMENT.  THIS  SOFTWARE IS PROVIDED  ON AN "AS  IS" BASIS,
 # AND  THE  AUTHOR  AND  DISTRIBUTORS  HAVE  NO  OBLIGATION  TO  PROVIDE
 # MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 #
 
 #
 # 0.01 2016-01-04   new: coneMiter, many thanks to Peter Dalhoff
 #
 #
 
 
  ###########################################################################
  #
  #                 I  -  N  -  I  -  T                        -  Application
  #
  ###########################################################################
  
    
    package provide tubeMiter 0.01
    
    package require vectormath 0.8
  
    namespace eval tubeMiter {
            #
        variable CONST_PI [ expr 4*atan(1) ]
            #
    }

    #-------------------------------------------------------------------------
        #  create coneMiter
        #
        #                              
        #                                |<-->|  frustum_TopRadius [mm]
        #                                |    | 
        #                           +----+----+  -------------------------
        #          miterAngle [°]  /     |     \                        ^
        #                         /      |      \                       |
        #         -----   +------+       |       \                      |
        #          ^     /\       +      |        \                     |
        #          |     \/        +     |         \                    | 
        #          |   - / - - - - + - - o          \  -----------      |
        #          |     |        +      |           \          ^       | frustum_Height [mm]
        #          V     \      +        |            \         |       |
        #         -----   +-+ °          |             \        | hks   |   
        #    tubeRadius    /             |              \       |       |
        #       [mm]      /              |               \      V       V
        #                +---------------+----------------+  -------------
        #                                |                |
        #                                |<-------------->|  frustum_BaseRadius [mm]
        #                              
        #
        # ... with many thanks to 
        #                      Peter Dalhoff
        #
        #
    proc tubeMiter::coneMiter {frustum_BaseRadius frustum_TopRadius frustum_Height tubeRadius miterAngle hks} {
                #
                # coneMiter $frustum_BaseRadius $frustum_TopRadius $frustum_Height $tubeRadius $miterAngle $hks
                #     ... previous   {R_Frustum_Base R_Frustum_Top H_Frustum R_Tube alpha hks}
                #
                #   frustum_BaseRadius ... Radius des Kegelstumpf unten
                #   frustum_TopRadius .... Radius des Kegelstumpf oben
                #   frustum_Height ....... Höhe des Kegelstumpf    
                #
                #   tubeRadius ........... Radius Zylinder in mm
                #   miterAngle ................ Schnittwinkel in grad (FrustumTop -> Joint <- Tube)
                #   hks .................. Höhe von Kegelfuß bis Achsenschnittpunkt
                #
                #
            variable CONST_PI    
                #
            set pi $CONST_PI
                #
                # Eingabedaten
                #
            set frustum_BaseRadius  [expr 1.0 * $frustum_BaseRadius]
            set frustum_TopRadius   [expr 1.0 * $frustum_TopRadius]
            set frustum_Height      [expr 1.0 * $frustum_Height]
                #
            set tubeRadius          [expr 1.0 * $tubeRadius]        ;# Radius Zylinder in mm
                #
            set alpha               [expr $miterAngle * $pi / 180]       ;# Schnittwinkel in rad
            set hks                 [expr 1.0 * $hks]               ;# Höhe von Kegelfuß bis Achsenschnittpunkt
                #
                #
                # Dies wird umgerechnet in eine Funktion Kegelradius in Abhängigkeit der Koordinate z2. 
                # Zunächst Geradengleichung bestimmen
            set a2 [expr $frustum_BaseRadius + ($frustum_TopRadius - $frustum_BaseRadius) * $hks / $frustum_Height]
            set b2 [expr ($frustum_TopRadius - $frustum_BaseRadius) / $frustum_Height]
                #
                # Wertebereich für Z2 ermitteln. 
                # Z2 läuft entlang der Kegelachse vom Achsenschnittpunkt aus positiv nach oben
                # Z1 läuft entlang der Zylinderachse. Vom Achsenschnittpunkt aus von links nach 
                # rechts ist positiv. Daher werden im Ergebnis negative Werte ausgespuckt, denn 
                # die Schnittkurve liegt ja links vom Achsenschnittpunkt. 
                # Oberer Sattelpunkt
                #
            set Z2_top [expr (   $tubeRadius+$a2*cos($alpha))/(-1*$b2*cos($alpha)+sin($alpha))]
                # unterer Sattelpunkt
            set Z2_bot [expr (-1*$tubeRadius+$a2*cos($alpha))/(-1*$b2*cos($alpha)+sin($alpha))]
                #
                #
            # puts ""
            # puts "pi              [format {%3.8f} $pi]"
            # puts ""
            # puts "tubeRadius:         [format {%3.8f} $tubeRadius]"
            # puts "alpha:          [format {%3.8f} $alpha]"
            # puts "hks:            [format {%3.8f} $hks]"
            # puts "frustum_BaseRadius: [format {%3.8f} $frustum_BaseRadius]"
            # puts "frustum_TopRadius:  [format {%3.8f} $frustum_TopRadius]"
            # puts "frustum_Height:      [format {%3.8f} $frustum_Height]"
            # puts "a2:             [format {%3.8f} $a2]"
            # puts "b2:             [format {%3.8f} $b2]"
            # puts ""
                #
                #
                # Durchführung der Berechnungen mit Hilfsebenen, die senkrecht zur Kegelachse verlaufen. Vom oberen Sattel bis zum unteren sind es nz2max+1 Hilfsebenen. Der Laufparameter dafür ist nz2 und zu jedem nz2 wird die entsprechende z2-Koordinate ermittelt, also in welchem Abstand sich die Hilfsebene zum Achsenschnittpunkt befindet. 
                # Dabei wandert der Winkel PHI1 für den Zylinder zwischen 0 und pi. Später wird das Ganze gespiegelt, um den Bereich zwischen pi und 2pi abzudecken. PHI1s ist eine Berechnung von PHI1, allerdings fehlt dort die korrekte Berücksichtigung des Quadranten, da die Arkusfunktion dies nicht kann. In einem weiteren Schritt wird dann der Quadrant festgestellt, um auch Werte >pi/2 erhalten zu können.
                # PHI2 ist der Umlaufwinkel am Kegel, siehe auch Skizze in der theoretischen Herleitung. 
                #
            set nz2max 400
            set nz2max 40
            set nz2 1
                # puts "\n"
                # puts "          \$nz2max .... $nz2max"
                # puts "          \$nz2 ....... $nz2"
                #
            array set Z1    {}
            array set Z2    {}
            array set PHI2  {}
            array set PHI1s {}
                #
            while {$nz2 <= ($nz2max + 1)} {
                # puts ""
                # puts "---- $nz2 -------------------------------------------"
                    # Z2(nz2)=Z2_top-(nz2-1)/nz2max*(Z2_top-Z2_bot);
                set Z2($nz2)        [expr $Z2_top - (1.0*$nz2-1) / $nz2max * ($Z2_top - $Z2_bot)]
                    # puts "       Z2($nz2):     [format {%3.8f} $Z2($nz2)]"
                    #
                    #
                    # Z1(nz2)=-sqrt((a2+b2*Z2(nz2))^2+Z2(nz2)^2-tubeRadius^2);
                set Z1($nz2)        [expr -1 * sqrt(pow(($a2+$b2*$Z2($nz2)),2) + pow($Z2($nz2),2)- pow($tubeRadius,2))]
                    # puts "       Z1($nz2):     [format {%3.8f} $Z1($nz2)]"
                    #
                    #
                    # PHI2(nz2)=acos((-Z2(nz2)*cos(alpha)-Z1(nz2))/((a2+b2*Z2(nz2))*sin(alpha)));
                set value           [expr (-1 * $Z2($nz2) * cos($alpha) - $Z1($nz2)) / (($a2 + $b2 * $Z2($nz2)) * sin($alpha))]
                if {$value < -1 || $value > 1} {
                    set roundedValue [format {%3.8f} $value]
                    if {abs($roundedValue) > 1.0} {
                        puts "\n"
                        puts "          <E> tcl expr error: $value"
                        puts "\n"
                        return -1
                    } else {
                        puts "          <I> tcl expr error: $value"
                        set value $roundedValue
                    }
                }
                set PHI2($nz2)      [expr acos($value)]
                    # puts "       PHI2($nz2):   [format {%3.8f} $PHI2($nz2)]"
                    #
                    #
                    # PHI1s(nz2)=asin((a2+b2*Z2(nz2))*sin(PHI2(nz2))/tubeRadius);
                set PHI1s($nz2)     [expr asin(($a2 + $b2 * $Z2($nz2)) * sin($PHI2($nz2)) / $tubeRadius)]
                    # puts "       PHI1s($nz2):  [format {%3.8f} $PHI1s($nz2)]"
                    #
                    #
                if {$nz2 > 2} {
                    set nz2_1 [expr $nz2 - 1]
                    set nz2_2 [expr $nz2 -2]
                    
                    if { (2 * $PHI1($nz2_1) - $PHI1($nz2_2)) < $pi/2} { 
                            # puts "       -> if:"
                        set PHI1($nz2) $PHI1s($nz2)
                    } else {
                            # puts "       -> else:"
                        set PHI1($nz2) [expr $pi - $PHI1s($nz2)]
                    }
                } else  {
                    set PHI1($nz2) $PHI1s($nz2)
                } 
                    # puts "       PHI1($nz2):   [format {%3.8f} $PHI1($nz2)]"
                    #
                    #
                    # end loop and incr nz2
                    #
                incr nz2 1
                    #
            }
            
                # puts "  -> \$Z2(1) ... $Z2(1)"
                # puts "  -> \$Z2(2) ... $Z2(2)"
            
            # puts "\n\n"
            # puts "---- mirror -------------------------"
                #
                # 1 ... add mirror
                # 0 ... dont add mirror
                #
            if {0} {
                while {$nz2 <= (2 * $nz2max + 1)} {
                        #
                    # puts $nz2
                        #
                        # Z1(nz2)=Z1(2*nz2max-nz2+2);
                    set nz2_id [expr 2 * $nz2max - $nz2 + 2]
                    set Z1($nz2) $Z1($nz2_id)
                        # puts "       Z1($nz2):     [format {%3.8f} $Z1($nz2)]"
                        #
                        #
                        # PHI1(nz2)=2*%pi-PHI1(2*nz2max-nz2+2);
                    set PHI1($nz2) [expr 2 * $pi - $PHI1($nz2_id)]
                        # puts "       PHI1($nz2):   [format {%3.8f} $PHI1($nz2)]"
                        #
                        # end loop and incr nz2
                        #
                    incr nz2 1
                        #
                }
            }
                #
            set coneMiter {}
            set coneMiterProfile {}
            foreach key [lsort -integer -decreasing [array names PHI1]] {
                # puts "   -> $key"
                set i_perimeter     [expr -1.0 * $PHI1($key) * $tubeRadius]
                set i_profile       [expr -1.0 * cos($PHI1($key)) * $tubeRadius]
                set i_miterOffset   $Z1($key)
                lappend coneMiter           $i_perimeter $i_miterOffset
                lappend coneMiterProfile    $i_profile   $i_miterOffset
            }
            set coneMiterMirror  {}
            set tmpList $coneMiter
            foreach {y x} [lreverse $tmpList] {
                set x [expr -1.0 * $x]
                lappend coneMiter           $x $y
            }
                #
                # 
            set coneMiterProfile [vectormath::rotatePointList {0 0} $coneMiterProfile 270]
                #
            return [list $coneMiter $coneMiterProfile]
                #    
    }
    #-------------------------------------------------------------------------
        #  create cylinderMiter
        #
        #                              
        #                           +--------+--------+
        #        miterAngle [°]     |        |        |
        #                           |        |        |
        #         ----    +---------+        |        |
        #          ^     /\          \       |        |
        #          |     \/           |      |        |
        #          |   - / - - - - - -+- - - o        |
        #          |     |            |      |        |
        #          V     \           /       |        |
        #         ----    +---------+        |        |
        #                           |        |        |
        #    tubeRadius [mm]        |        |        |
        #                           +--------+--------+
        #                                    |        |
        #                                    |<------>|    cylinderRadius [mm]
        #                                       
        #
        #
        # from bikeGeometry::tube_intersection
        #
    proc tubeMiter::cylinderMiter { cylinderRadius tubeRadius miterAngle {side {right}} {offset {0}}  {startAngle {0}}  {opposite {no}}} {
                #
            # puts " --< tubeMiter::cylinderMiter >--"
                #
            if {$opposite != {no} } {
                set miterAngle    [expr 180 - $miterAngle]
                        # puts "       -> intersection_angle $miterAngle"
            }
                #
            set loops            72
            set angleIncr [expr 360 / $loops]
                #
            set angle           -180
            set cylinderMiter   {}
            while {$angle <= 180} {
                        # puts "  -> \$angle $angle"
                    set rad_Angle   [vectormath::rad [expr $angle -$startAngle]]
                    set pp [expr 2*$tubeRadius*$vectormath::CONST_PI*$angle/360 ];   # -- position on perimeter
                        #
                    set h [expr $offset + $tubeRadius*sin($rad_Angle)];              # -- y-value on circle 
                    set b [expr $tubeRadius*cos($rad_Angle)];                        # -- x-value on circle
                        #
                    if {[expr abs($cylinderRadius)] >= [expr abs($h)]} {
                        set y  [expr sqrt(pow($cylinderRadius,2) - pow($h,2))] ;     # -- point on miter-curve based on perimeter position
                    } else {
                        set y  0                                             ;       # -- undefined for situations where point on circle does not cut sectioning tube
                    }
                        #
                    if {[expr abs($miterAngle)] != 90.0} {
                        set v1 [expr $b/tan([vectormath::rad $miterAngle])]; # -- tube - component of offset
                        set v2 [expr $y/sin([vectormath::rad $miterAngle])]; # -- intersecting tube component of offset
                        set y  [expr $v1 + $v2]
                    }
                        #
                    set xy [list $pp $y]
                    set xy    [vectormath::rotatePoint {0 0} $xy  180]
                    if {$side == {left}}  {
                        lappend cylinderMiter [lindex $xy 0] [lindex $xy 1]
                    } else {
                        lappend cylinderMiter [expr -1 * [lindex $xy 0]] [lindex $xy 1]
                    }
                    set angle       [expr $angle + $angleIncr]
            }
                #
                # -- miter Profile
                #
            set angle 90
            set coordList {}
                #
                #
            while {$angle >= -90} {
                # debug tubeMiter - 2016.01.04
                #    ... check for "Debug_1.69" in bikeGeometry
                #
            # foreach angle {90 60 30 10 0 -10 -30 -60 -90} {}
                set rad_Angle   [vectormath::rad $angle]
                set r1_x        [expr $tubeRadius*cos([vectormath::rad [expr 90+$angle]]) ]
                set r1_y        [expr $tubeRadius*sin([expr 1.0*(90-$angle)*$vectormath::CONST_PI/180]) + $offset]
                if {[expr abs($cylinderRadius)] >= [expr abs($r1_y)]} {
                    set cut_perp    [expr sqrt(pow($cylinderRadius,2) - pow($r1_y,2)) ]
                } else {
                    set cut_perp     0
                }
                set cut_angle   [expr $cut_perp / sin([vectormath::rad $miterAngle]) ]
                set cut_angOff  [expr $r1_x / tan([vectormath::rad $miterAngle]) ]
                set cut_eff     [expr $cut_angle + $cut_angOff ]
                set xy  [list $r1_x $cut_eff]
                if {$side != {left}}  {}
                set xy  [vectormath::rotatePoint {0 0} $xy  180]
                lappend coordList [lindex $xy 0] [lindex $xy 1]
                    #
                set angle [expr $angle - $angleIncr]
            }
                #
            if {$side != {left}}  {
                set cylinderMiterProfile [vectormath::rotatePointList {0 0} $coordList 270]
            } else {
                set cylinderMiterProfile [vectormath::rotatePointList {0 0} $coordList 270]
            }
                #
            return [list $cylinderMiter $cylinderMiterProfile]
                #
    }

