 ##+##########################################################################te
 #
 # package: rattleCAD_AddOn   
 #
 #   canvasCAD is software of Manfred ROSENBERGER
 #       based on tclTk, BWidgets and tdom on their
 #       own Licenses.
 #
 # Copyright (c) Manfred ROSENBERGER, 2015/01/23
 # 
 # IN NO  EVENT SHALL THE AUTHOR  OR DISTRIBUTORS BE LIABLE  TO ANY PARTY
 # FOR  DIRECT, INDIRECT, SPECIAL,  INCIDENTAL, OR  CONSEQUENTIAL DAMAGES
 # ARISING OUT  OF THE  USE OF THIS  SOFTWARE, ITS DOCUMENTATION,  OR ANY
 # DERIVATIVES  THEREOF, EVEN  IF THE  AUTHOR  HAVE BEEN  ADVISED OF  THE
 # POSSIBILITY OF SUCH DAMAGE.
 #
 # THE  AUTHOR  AND DISTRIBUTORS  SPECIFICALLY  DISCLAIM ANY  WARRANTIES,
 # INCLUDING,   BUT   NOT  LIMITED   TO,   THE   IMPLIED  WARRANTIES   OF
 # MERCHANTABILITY,    FITNESS   FOR    A    PARTICULAR   PURPOSE,    AND
 # NON-INFRINGEMENT.  THIS  SOFTWARE IS PROVIDED  ON AN "AS  IS" BASIS,
 # AND  THE  AUTHOR  AND  DISTRIBUTORS  HAVE  NO  OBLIGATION  TO  PROVIDE
 # MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 #
 # ---------------------------------------------------------------------------
 #  namespace:  rattleCAD_AddOn
 # ---------------------------------------------------------------------------
 #
 #
        # -- Libraries  ---------------
        #
    package require Tk
        #
    package provide rattleCAD_AddOn    0.00
        #
        #
    package require mockup_3D
    package require reynolds_FEA
        #
        #
        # --------------------------------------------
        #
    namespace eval rattleCAD_AddOn {
                #
            variable packageHomeDir [file normalize [file join [pwd] [file dirname [file dirname [info script]]]] ]
                #
            variable cfg_Position   {}
                #
            variable fileName
            variable sourceText
            variable targetText     
                #
            variable targetContent  {}
                #
            variable exportFileName
            variable exportDir
            
            variable exportFileExtension {scad}    
            variable rattleCAD_DOM
            
            variable importDir
            
            variable FrontHubWidth   100.00
            variable RearHubWidth    200
            variable ChainStayOffset 50
                
            variable RearDropout;   array set RearDropout {}
            variable HandleBar;     array set HandleBar {}
            variable Saddle;        array set Saddle {}
            
            set      fileName       {}
            
            variable colorFrame     gold
            variable colorComponent LightGray
            variable colorTyre      DimGray
            variable colorSaddle    DimGray
            variable colorHandleBar DimGray
                #
                #
    }    

    namespace eval rattleCAD_AddOn {
            namespace ensemble create -command ::rattleCAD_AddOn \
                -map {
                        create     create
                    } 
    }
 
    #    {command "&Export openSCAD" {}  "Export to openSCAD"    {Ctrl O}      -command { rattleCAD::view::gui::export_openSCAD } }
    #    {command "&Export Reynolds FEA" {} "Export Reynolds FEA" {Ctrl f}     -command { rattleCAD::view::gui::export_reynoldsFEA  1.0} }

    
    
    proc rattleCAD_AddOn::init_configValues {} {
                #
                #
            return
                #
	}
    
    
    #-------------------------------------------------------------------------
       #  create toplevel widget
       #
     proc rattleCAD_AddOn::create {} {
                # 
            variable cfg_Position 
                #
            set master . 
			set w    .addOn
			
                # -----------------
                # master window information
            set root_xy [split  [wm geometry $master] +]
            set root_w  [winfo width $master]
            set root_x  [lindex $root_xy 1]
            set root_y  [lindex $root_xy 2]
                #
            set pos_x   [expr $root_x -  80]
            set pos_y   [expr $root_y - 200]
                #
            set cfg_Position [list $root_x $root_y $root_w [expr $root_x+8+$root_w] 0 ]
                #
                # -----------------
                # check if window exists
            if {[winfo exists $w]} {
                    # restore if hidden
                    # puts "   ... $w allready exists!"
                wm deiconify    $master
                focus           $master
                return
            }
                #
                # -----------------
                # create a toplevel window to edit the attribute values
                #
            toplevel    $w
                #
                # create iconBitmap  -----
            if {$::tcl_platform(platform) == {windows}} {
                wm iconbitmap $w [file join $::APPL_Config(BASE_Dir) tclkit.ico]
            } else {
                wm iconphoto  $w [image create photo .ico1 -format gif -file [file join $::APPL_Config(BASE_Dir)  icon16.gif] ]
            }
                #
                # -----------------
                # create content
            fill_GUI        $w
                #
                # -----------------
                #
            bind $w         <Configure> [list [namespace current]::register_relative_position     $master $w]
            bind $master    <Configure> [list [namespace current]::reposition_to_master           $master $w]
                #
                #
            wm deiconify    $master
                #
            wm title        $w "rattleCAD AddOn"
            wm geometry     $w +[expr $root_x+600]+[expr $root_y+200]
                # wm attributes   $w -toolwindow 
            wm transient    $w $master 
                #
            set w_size      [wm sizefrom $w]
            puts "   .... WindowSize:  [wm sizefrom $w]"
                #
            focus           $w
                #
            return
                #
    }


    #-------------------------------------------------------------------------
       #  create config Content
       #
    proc rattleCAD_AddOn::fill_GUI {w} {
                #
            variable compCanvas
                #
                # -----------------
                #   clean berfoe create
                # -----------------
                #   create notebook
            pack [  frame $w.nb_Continer ]
                #
            set nb_Config   [ ttk::notebook $w.nb_Continer.nb ]
            pack $nb_Config     -expand no -fill both
                #
            $nb_Config add [frame $nb_Config.openSCAD]      -text "openSCAD - 3D Mockup"
            $nb_Config add [frame $nb_Config.reynoldsFEA]   -text "  Reynolds FEA  "
                #
                # -----------------
                # add content
            ::mockup_3D     insert_ControlWidget $nb_Config.openSCAD
            ::reynolds_FEA  insert_ControlWidget $nb_Config.reynoldsFEA
                #
				# -----------------
                #			
		    # bind $nb_Config <<NotebookTabChanged>> [list [namespace current]::bind_notebookTabChanged  $nb_Config]
				#
                # -----------------
                #
            # wm resizable    $w  0 0
                #
                #
            $nb_Config      select 0
                #
            return
    }


    #-------------------------------------------------------------------------
       #  postion config panel to master window
       #
    proc rattleCAD_AddOn::reposition_to_master {master w} {

            variable cfg_Position

            if {![winfo exists $w]} return

            # wm deiconify   $w

            set root_xy [split  [wm geometry $master] +]
            set root_w    [winfo  width $master]
            set root_x    [lindex $root_xy 1]
            set root_y    [lindex $root_xy 2]

            set update no

            if {$root_x != [lindex $cfg_Position 0]} {set update yes}
            if {$root_y != [lindex $cfg_Position 1]} {set update yes}
            if {$root_w != [lindex $cfg_Position 2]} {set update resize}

            switch $update {
                yes {
                        set dx [lindex $cfg_Position 3]
                        set dy [lindex $cfg_Position 4]
						  # puts "   -> reposition_to_master  - $w +[expr $root_x+$dx]+[expr $root_y+$dy]"
                        catch {wm geometry    $w +[expr $root_x+$dx]+[expr $root_y+$dy]}
                    }
                resize {
                        set d_root [expr $root_w - [lindex $cfg_Position 2]]
                        set dx [ expr [lindex $cfg_Position 3] + $d_root ]
                        set dy [lindex $cfg_Position 4]
                        catch {wm geometry    $w +[expr $root_x+$dx]+[expr $root_y+$dy]}
                }
            }
    }
    #-------------------------------------------------------------------------
       #  register_relative_position
       #
    proc rattleCAD_AddOn::register_relative_position {master w} {

            variable cfg_Position

            set root_xy [split  [wm geometry $master] +]
            set root_w  [winfo width $master]
            set root_x  [lindex $root_xy 1]
            set root_y  [lindex $root_xy 2]
                # puts "    master: $master: $root_x  $root_y"

            set w_xy [split  [wm geometry $w] +]
                # puts "    w   .... $w_xy"
            set w_x [lindex $w_xy 1]
            set w_y [lindex $w_xy 2]
                # puts "    w   ..... $w: $w_x  $w_y"
            set d_x     [ expr $w_x-$root_x]
            set d_y     [ expr $w_y-$root_y]
                # puts "    w   ..... $w: $d_x  $d_y"
                # puts "    w   ..... $root_x $root_y $d_x $d_y"
            set cfg_Position [list $root_x $root_y $root_w $d_x $d_y ]
                # puts "     ... register_relative_position $cfg_Position"
    }
